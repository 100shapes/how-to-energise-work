---
title: Our Mission & Vision
contentType: Entry
description: >
  Here's a description of the world we're trying to create.
---

"Manage your energy, not your time".

We're trying to create a world of work which focuses on maintaining the energy of employees, not micro-managing their time.



