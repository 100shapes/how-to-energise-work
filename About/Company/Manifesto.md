---
title: >
  ‘Energising Work’ Manifesto
description: >
  We're on a mission to "energise work for everyone". Let's talk about what that means and describe how we plan to do it.
contentType: Entry
author: michele@100shapes.com
---

## What is ‘energising work’?

For us at 100 Shapes, work is "energised" when it fits with who you are, what you believe in and where it's taking you.

### Fit

Work that
- Meets the strategic objective of the organisation
